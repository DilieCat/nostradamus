const express = require("express")
const assert = require("assert")
const router = express.Router()
const db = require("../db/mysql-connector");
const jwt = require("../helpers/jwt")
const logger = require("tracer").colorConsole();


class Queryhandler {

    constructor() {}

    executeQuery(query, cb) {

        try {
            logger.debug(query)
            // Perform query
            db.query(query, (err, rows, fields) => {
                if (err) {
                    logger.error(err);
                    cb(err, rows)
                } else {
                    logger.info(rows)
                    logger.debug("err")
                    cb(null, rows)
                }
            });


        } catch (ex) {
            next(ex);
        }
    }

}

module.exports = Queryhandler