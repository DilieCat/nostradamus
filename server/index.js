const config = require("./config/config.json");
const express = require("express");
const bodyParser = require("body-parser");
const apiv1 = require("./routes/apiv1");
const auth = require("./routes/auth");
const logger = require("tracer").colorConsole();
const loggerFile = require("tracer").dailyfile({
  root: "./logs",
  maxLogFiles: 10,
  allLogsFileName: "nostradamusclock",
  format: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})",
  dateformat: "HH:MM:ss.L"
});

const app = express();
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Middelware, logging voor alle request
app.all("*", function (req, res, next) {
  loggerFile.info("%s", req.hostname, req.headers, req.body);
  next();
});

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, x-access-token');
  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
  res.sendStatus(200);
  } else {
  next();
  }
 });

// Routing without JWT
app.use("/auth", auth);

// Routing protected by JWT
app.use("/apiv1", apiv1);
//app.use("/apiv2", apiv2);

// Optional log error
function errorLoggerHandler(err, req, res, next) {
  loggerFile.error("%s", err.message);
  next(err);
}

//Log running environment
logger.debug(process.env.NODE_ENV);
//Handle production

    //Handle SPA
app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'))
app.use(express.static(__dirname + '/public/'));

// Set default error handler
function errorResponseHandler(err, req, res, next) {
  res.status(500);
  res.json({
    msg: "Gaet Heen Schavuit!"
  });
}

// Register the error handlers
app.use(errorLoggerHandler);
app.use(errorResponseHandler);

// ECMA 6
const port = process.env.PORT || config.app.port;
const server = app.listen(port, () => {
  logger.info(
    "The Nostradamus Clock app, the magic happens at port " + server.address().port
  );
});

module.exports = app;