const express = require("express")
const assert = require("assert")
const router = express.Router()
const jwt = require("../helpers/jwt")
const logger = require("tracer").colorConsole();
const db = require("../db/mysql-connector");
const Session = require("../models/session")
const Queryhandler = require("../helpers/queryhandler")
const _queryhandler = new Queryhandler()
const Utils = require("../helpers/utils")

var token;
var sessionId;
var startTime;

// Check voor alle endpoints het token
router.all("*", (req, res, next) => {
  assert(
    typeof req.header("x-access-token") == "string",
    "Token is not a string!"
  );

  token = req.header("x-access-token") || "";


  jwt.decodeToken(token, (err, payload) => {
    if (err) {
      logger.error("Error handler: " + err.message);
      next(err);
    } else {
      next();
    }
  });
});

//Create session
router.post("/session", function (req, res, next) {

  try {
    // var today = new Date();
    // var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    // var dateTime = date + ' ' + time;

    logger.debug("ClockNr: ", jwt.decode(token).clocknr)

    startTime = new Date(req.body.starttime);

    const query = {
      sql: "INSERT INTO `Session`(clockNr, departmentId, startTime) VALUES (?,?,?); SELECT LAST_INSERT_ID() AS 'LAST_INSERT_ID';",
      values: [jwt.decode(token).clocknr, req.body.departmentid, req.body.starttime],
      timeout: 2000
    }

    _queryhandler.executeQuery(query, (err, result) => {
      if (err) {
        res.status(500).json(err.toString());
      } else {
        res.status(200).json(result);
        var result = (JSON.parse(JSON.stringify(result[1])))
        sessionId = (result[0].LAST_INSERT_ID);
      }
    })
  } catch (ex) {
    next(ex)
  }
});

//Get all departments
router.get("/department", (req, res, next) => {

  try {
    const query = {
      sql: `SELECT * FROM Department;`,
      timeout: 2000
    };

    _queryhandler.executeQuery(query, (err, result) => {
      if (err) {
        res.status(500).json(err.toString());
      } else {
        res.status(200).json(result);
      }
    })
  } catch (ex) {
    next(ex)
  }
});

//Create new department
router.post("/department", (req, res, next) => {
  assert(typeof req.body.name === "string", "Name is not a string!");

  try {
    const query = {
      sql: `INSERT INTO Department (Name) VALUES (?)`,
      values: [req.body.name],
      timeout: 2000
    };

    _queryhandler.executeQuery(query, (err, result) => {
      if (err) {
        res.status(500).json(err.toString());
      } else {
        res.status(200).json(result);
      }

    });

    //queryhandler.executeQuery("SELECT SessionId from `Session` WHERE clockNr = ?")
  } catch (ex) {
    next(ex)
  }
});

//Update session with given id, insert endtime and worktime
router.put("/session", function (req, res, next) {
  try {
    var endTime = new Date(req.body.endtime);
    logger.debug("Endtime:" + endTime)
    logger.debug("Starttime:" + startTime)
    var workTime = endTime - startTime;
    workTime = Utils.secondsToTime(workTime)
    logger.debug("Worktime:" + workTime)


    const query = {
      sql: "UPDATE `Session` SET endTime = ?, workTime = ? WHERE sessionId = ?",
      values: [endTime, workTime, sessionId],
      timeout: 2000
    };
    _queryhandler.executeQuery(query, (err, result) => {
      if (err) {
        res.status(500).json(err.toString());
      } else {
        res.status(200).json(result);
      }

    });

  } catch (ex) {
    next(ex)
  }
});

//Delete apartment by id
router.delete("/department/:id", (req, res, next) => {
  const departmentId = req.params.id;

  try {
    const query = {
      sql: `DELETE FROM Department WHERE Department.DepartmentId=?`,
      values: [departmentId],
      timeout: 2000
    };

    _queryhandzler.executeQuery(query, (err, result) => {
      if (err) {
        res.status(500).json(err.toString());
      } else {
        res.status(200).json(result);
      }

    });

  } catch (ex) {
    next(ex)
  }
});

router.post("/break", function (req, res, next) {
  try {
    console.log("insertBreak: called")
    const breaks = new Break(req.body.sessionid, req.body.starttime);
    const query = {
      sql: `INSERT INTO Break (SessionId, StartTime) VALUES (?,?)`,
      values: [breaks.sessionId, breaks.startTime],
      timeout: 5000
    }
    _queryhandler.executeQuery(query, (err, result) => {
      if (err) {
        res.status(500).json(err.toString());
      } else {
        res.status(200).json(result);
      }

    });
  } catch (ex) {

    next(ex)
  }
});

// Fall back, display some info
router.all("*", (req, res) => {
  res.status(200);
  res.json({
    description: "Nostradamus Clock API version 1"
  })
});


module.exports = router;