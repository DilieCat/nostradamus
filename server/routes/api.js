const express = require("express");
const router = express.Router();
const AuthController = require('../Controller/authentication.controller')
const validate = [AuthController.validateToken]


router.post('/login', AuthController.login);

module.exports = router;