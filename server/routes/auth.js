const express = require("express");
const assert = require("assert");
const router = express.Router();
const db = require("../db/mysql-connector");
const bcrypt = require("bcryptjs");
const jwt = require("../helpers/jwt");
const logger = require("tracer").colorConsole();
const Person = require("../models/person");

const Queryhandler = require("../helpers/queryhandler")
const _queryhandler = new Queryhandler();

const _person = new Person();

const saltRounds = 10;

//
// Register new user
//
router.post("/register", (req, res, next) => {

  // Validate with assert is string etc ..
  assert(typeof req.body.clocknr === "string", "ClockNr name is not a string!");
  assert(typeof req.body.roleid === "string", "RoleId is not a string!");
  assert(typeof req.body.email === "string", "Email is not a string!");
  _person.validateEmail(req.body.email);
  assert(typeof req.body.personname === "string", "Person name is not a string!");
  assert(typeof req.body.password === "string", "Password is not a string!");

  // Create new user object, hash password (do not store in db).
  // Throws err if no valid object can be constructed
  const hash = bcrypt.hashSync(req.body.password, saltRounds);

  var query;
  // Construct query object
  if (req.body.status == null) {
    query = {
      sql: "INSERT INTO `Person`(ClockNr, RoleId, Email, PersonName, Password) VALUES (?,?,?,?,?)",
      values: [req.body.clocknr, req.body.roleid, req.body.email, req.body.personname, hash],
      timeout: 2000
    };


  } else {

    // Construct query object
    query = {
      sql: "INSERT INTO `Person`(ClockNr, RoleId, Email, PersonName, Password, Status) VALUES (?,?,?,?,?,?)",
      values: [req.body.clocknr, req.body.roleid, req.body.email, req.body.personname, hash, req.body.status],
      timeout: 2000
    };
  }

  _queryhandler.executeQuery(query, (err, result) => {
    if (err) {
      res.status(500).json(err.toString());
    } else {
      res.status(200).json(result);
    }

  });

});

//
// Login with email / password
//
router.post("/login", (req, res, next) => {
  try {
    // Validate with assert is string etc ..
    assert(typeof req.body.personname === "string", "PersonName is not a string!");
    assert(typeof req.body.password === "string", "Password is not a string!");

    // Construct query object
    const query = {
      sql: "SELECT Password, ClockNr, Email FROM `Person` WHERE `PersonName`=?",
      values: [req.body.personname],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        next(err);
      } else {
        if (
          rows.length === 1 &&
          bcrypt.compareSync(req.body.password, rows[0].Password)
        ) {
          token = jwt.encodeToken(rows[0].Email, rows[0].ClockNr);
          logger.debug("Token: " + token)
          res.status(200).json({
            token: token
          });
        } else {
          next(new Error("Invalid login, bye"));
        }
      }
    });
  } catch (ex) {
    logger.debug(ex)
    next(ex);
  }
});

// Fall back, display some info
router.all("*", (req, res, next) => {
  next(new Error("Unknown endpoint"));
});


module.exports = router;