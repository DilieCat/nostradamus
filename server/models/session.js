class Session {

    // constructor(clockNr, departmentId, startTime, endTime, workTime) {
    //     this.clockNr = clockNr,
    //     this.departmentId = departmentId,
    //     this.startTime = startTime,
    //     this.endTime = endTime,
    //     this.workTime = workTime
    // }

     constructor(sessionId, clockNr, departmentId, startTime, endTime, workTime) {
        this.sessionId = sessionId,
        this.clockNr = clockNr,
        this.departmentId = departmentId,
        this.startTime = startTime,
        this.endTime = endTime,
        this.workTime = workTime
    }
}

module.exports = Session;