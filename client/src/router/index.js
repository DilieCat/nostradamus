import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import ClockOut from '@/components/Clocking/ClockOut'
import ClockIn from '@/components/Clocking/ClockIn'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: {
        name: "Homepage",
      }
    },
    {
      path: '/homepage',
      name: 'Homepage',
      component: Homepage
    },
    {
      name: 'ClockOut',
      path: '/clockout',
      component: ClockOut
    },
    {
      name: 'ClockIn',
      path: '/clockin',
      component: ClockIn
    }
  ]
})
