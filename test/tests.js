var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/index.js');
var crypto = require("crypto");


chai.should();

chai.use(chaiHttp);

var id = crypto.randomBytes(20).toString('hex');
var idStatus = crypto.randomBytes(20).toString('hex');

var clocknr = "" + Math.round(Math.random() * (99999999 - 1) + 1)
var clocknrStatus = "" + Math.round(Math.random() * (99999999 - 1) + 1)


describe('Users/', () => {

    it('Register new user', (done) => {
        chai.request(server)
            .post('/auth/register')
            .set('content-type', 'application/json')
            .send({
                "clocknr": clocknr,
                "roleid": "1",
                "email": "jsnow@avans" + id + ".nl",
                "personname": "JonSnow" + id,
                "password": "yoloswaggins"
            })
            .end((err, res, body) => {
                res.should.have.status(200);
                done();
            })
    })

    it('Register new user with activity', (done) => {
        chai.request(server)
            .post('/auth/register')
            .set('content-type', 'application/json')
            .send({
                "clocknr": clocknrStatus,
                "roleid": "1",
                "email": "jsnow@avans" + idStatus + ".nl",
                "personname": "JonSnow" + idStatus,
                "password": "yoloswaggins",
                "status": true
            })
            .end((err, res, body) => {
                res.should.have.status(200);
                done();
            })
    })

    it('User succesfully login', (done) => {
        chai.request(server)
            .post('/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "personname": "JonSnow" + id,
                "password": "yoloswaggins"
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            })
    });

    it('User invalid login', (done) => {
        chai.request(server)
            .post('/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "personname": "test",
                "password": "test"
            })
            .end((err, res) => {
                res.should.have.status(500);
                done();
            })
    });
});
describe('Session/', () => {

    it('Clock in', (done) => {
        chai.request(server)
            .post('/apiv1/session')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NjA0Mjc2MTgsImlhdCI6MTU1OTU2MzYxOCwic3ViIjoiYWRtaW5AZXhhbXBsZS5jb20iLCJjbG9ja25yIjoxMTF9.QMFKPGnQwX9y_QEHPgvU86tadDL24HFRlWontJEvD3I', 'Content-Type', 'application/json')
            .send({
                "starttime": "2019-06-05 13:50:01",
                "departmentid": 1
            })
            .end((err, res, body) => {
                res.should.have.status(200);
                done();
            })
    })

    it('Clock out', (done) => {
        chai.request(server)
            .put('/apiv1/session')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NjA0Mjc2MTgsImlhdCI6MTU1OTU2MzYxOCwic3ViIjoiYWRtaW5AZXhhbXBsZS5jb20iLCJjbG9ja25yIjoxMTF9.QMFKPGnQwX9y_QEHPgvU86tadDL24HFRlWontJEvD3I', 'Content-Type', 'application/json')
            .send({
                "endtime": "2019-07-05 13:50:01"
            })
            .end((err, res, body) => {
                res.should.have.status(200);
                done();
            })
    })
});

// describe('Break/', () => {

//     it('User successful insert break', (done) => {
//         chai.request(server)
//             .post('/break')
//             .set('Content-Type', 'application/json')
//             .send({
//                 "sessionid": "1",
//                 "starttime": "2019-29-05 11:00:00"
//             })
//             .end((err, res) => {
//                 res.should.have.status(200);
//                 done();
//             })
//     });

//     it('User invalid insert break', (done) => {
//         chai.request(server)
//             .post('/break')
//             .set('Content-Type', 'application/json')
//             .send({
//                 "sessionid": "0",
//                 "starttime": "2019-29-05 11:00:00"
//             })
//             .end((err, res) => {
//                 res.should.have.status(500);
//                 done();
//             })
//     })
//};

describe('Apartment/', () => {
    it('Get all departments', (done) => {
        chai.request(server)
            .get('/apiv1/departement')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTk5OTI1MzgsImlhdCI6MTU1OTEyODUzOCwic3ViIjoiYWRtaW4ifQ.5RGRrcvJzbrEK4H-O9wZxyq6mZum6HqH08Nfju_NsnM')
            .end((err, res) => {
                res.should.have.status(200);
                done()
            })
    })

    it('Post new department', (done) => {
        chai.request(server)
            .post('/apiv1/departement')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTk5OTI1MzgsImlhdCI6MTU1OTEyODUzOCwic3ViIjoiYWRtaW4ifQ.5RGRrcvJzbrEK4H-O9wZxyq6mZum6HqH08Nfju_NsnM', 'content-type', 'application/json')
            .send({
                "name": "testius"
            })
            .end((err, res, body) => {
                res.should.have.status(200);
                done();
            })
    })

    it('Delete department with id 6', (done) => {
        chai.request(server)
            .delete('/apiv1/departement/6')
            .set('x-access-token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTk5OTI1MzgsImlhdCI6MTU1OTEyODUzOCwic3ViIjoiYWRtaW4ifQ.5RGRrcvJzbrEK4H-O9wZxyq6mZum6HqH08Nfju_NsnM')
            .end((err, res, body) => {
                res.should.have.status(200);
                done();
            })
    })
});